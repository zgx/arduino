/* 
 使板载Led亮一秒，灭一秒，如此往复。
 */
 
//大部分Arduino控制板上，数字13号引脚都有一颗Led。
int LED=13;

void setup() {
  // 初始化数字引脚，使其为输出状态。
  pinMode(LED,OUTPUT);
}

void loop() {
  digitalWrite(LED,HIGH);  // 使Led亮
  delay(500);              // 持续500ms
  digitalWrite(LED,LOW);   // 使Led灭
  delay(250);              // 持续250ms
}
